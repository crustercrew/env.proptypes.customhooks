import logo from './logo.svg';
import './App.css';
import Cobaprops from './component/coba';
import { Box } from '@mui/material';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Appbar from './component/appbar';
import Apilist from './pages/cardlist';
function App() {

  const apiUrl = process.env.REACT_APP_BASE_URL;

  const fechData = async () =>{
    try{
      const response = await fetch(apiUrl);
      const data = await response.json()
      console.log(data,'test');
    } catch (e){
      console.log(e)
    }
  }


  return (
<Box>
  <Router>
    <Appbar/>
    <Switch>
      <Route exact path="/">
        <Apilist/>
      </Route>
    </Switch>
  </Router>
</Box>
  );
}

export default App;
