import React,{useState} from 'react';
import CameraAltIcon from '@mui/icons-material/CameraAlt';
import { Tab, Tabs,AppBar,Toolbar,Container,useMediaQuery, useTheme, createTheme as theme } from '@mui/material';
import DrawerComponent from '../drawer';

const pages = ['Blog App', 'About', 'Pricing'];

function ResponsiveAppBar() {
  const [value, setValue] = useState();
  // const theme = useTheme();
  let createTheme = theme({
    palette: {
      primary: {
        main: process.env.REACT_APP_COLOR_PRIMARY,
      },
      secondary: {
        main: process.env.REACT_APP_COLOR_SECONDARY,
      },
      success: {
        main: process.env.REACT_APP_COLOR_TUMBNAIL,
      }
    },
  });
  const isMatch = useMediaQuery(createTheme.breakpoints.down('md'));

  return (
    <AppBar position="static" theme={createTheme}>
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <CameraAltIcon sx={{ display: { md: 'flex' }, mr: 1 }}  />
          {isMatch ? (
            
              <DrawerComponent/>
          ) : (
          <Tabs textColor='inherit' value={value} indicatorColor="secondary" onChange={(e, value) => setValue(value)}>
            {pages.map((page) => (<Tab label={page} />)
            )}
          </Tabs>
          )}
        </Toolbar>

      </Container>
    </AppBar>
  );
}
export default ResponsiveAppBar;