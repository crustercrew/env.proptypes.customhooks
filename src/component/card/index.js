import React from "react";
import {Grid,Typography,createTheme as theme} from '@mui/material'
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import Avatar from '@mui/material/Avatar';
import { red } from '@mui/material/colors';
import PropTypes from 'prop-types';

export default function CardList(props) {
    const {items} = props

    let createTheme = theme({
        palette: {
          primary: {
            main: process.env.REACT_APP_COLOR_PRIMARY,
          },
          secondary: {
            main: process.env.REACT_APP_COLOR_SECONDARY,
          },
          success: {
            main: process.env.REACT_APP_COLOR_TUMBNAIL,
          }
        },
      });

    return(
        <Grid container xs={8} md={6} justifyContent="center" alignItems="center">
        <Card sx={{ maxWidth:345,marginX:'15px',marginTop:'15px',justifyContent:'center', alignItems:'center' }}>
            <CardHeader avatar={
                <Avatar theme={createTheme} aria-label="recipe">
                    T
                </Avatar>
            }
            title={items.title}
            subheader={items.datePost}
            />
            <CardMedia
            component="img"
            height='194'
            image={items.img}
            alt="image splash"
            />
            <CardContent>
                <Typography variant="body2" color='text.secondary'>
                    {items.description}
                </Typography>
            </CardContent>
        </Card>
        </Grid>
    )
}
CardList.propTypes = {
    items: PropTypes.arrayOf(
        PropTypes.shape({
            title: PropTypes.string,
            datePost: PropTypes.string,
            img: PropTypes.string,
            description: PropTypes.string,
        })
    )
}
