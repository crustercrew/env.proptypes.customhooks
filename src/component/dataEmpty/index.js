import React from "react";
import {Box, Typography} from "@mui/material";

export default function DataEmpty(){
    
    return (
        <Box
          sx={{
            display: 'flex',
            height: '80vh',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
            }}
          >
            <Typography variant="h1" component="div" sx={{ mx: 'auto' }}>
              0
            </Typography>

            <Typography variant="h1" component="div" sx={{ mx: 'auto' }}>
              User
            </Typography>
          </Box>
        </Box>
    )
}