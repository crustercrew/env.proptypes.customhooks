import React, { Fragment, useState } from "react";
import { Drawer, IconButton, List, ListItemButton, ListItemText, ListItemIcon } from "@mui/material";
import MenuIcon from '@mui/icons-material/Menu';

const pages = ['Blog App', 'About', 'Pricing'];

export default function DrawerComponent() {
    const [openDrawer,setOpenDrawer]= useState(false); 
    return(
        <Fragment>
        <Drawer anchor="right" open={openDrawer} onClose={()=> setOpenDrawer(false)}>
            <List>
                {pages.map((page,index)=>(
                    <ListItemButton key={index}>
                        <ListItemIcon>
                            <ListItemText>{page}</ListItemText>
                        </ListItemIcon>
                    </ListItemButton>
                ))}
            </List>
        </Drawer>
        <IconButton sx={{ color: "white", marginLeft: "auto" }}
        onClick={() => setOpenDrawer(!openDrawer)}>
            <MenuIcon/>
        </IconButton>
        </Fragment>
    )
}