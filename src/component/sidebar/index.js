import React from "react";
import { Box } from "@mui/material";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Divider from "@mui/material/Divider";
import Avatar from "@mui/material/Avatar";

export default function Sidebar({ item, index }) {
  return (
    <Box sx={{ xs: "none", sm: "block" }}>
      <List
        sx={{
          width: "100%",
          maxWidth: 360,
        }}
      >
        <ListItem>
          <ListItemAvatar>
            <Avatar>{item.avatar}</Avatar>
          </ListItemAvatar>
          <ListItemText primaryTypographyProps={{ sx: { color: "red" } }} primary={item.name} secondary={item.date} />
        </ListItem>
        <Divider variant="inset" component="li" />
      </List>
    </Box>
  );
}
