import React,{useState,useEffect} from 'react';
import axios from 'axios';

export default function useAsync(apiUrl){
    const [listdata, setListData] = useState({});

    useEffect(()=>{
            const callApi = () => {
            axios
                .get(`${apiUrl}`, {})
                .then((res) => setListData(res.data))
                .catch((err) => {
                console.log(err);
                });
            };

            callApi();

    },[apiUrl]);

    return listdata;
}