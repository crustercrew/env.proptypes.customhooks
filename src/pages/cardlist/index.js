import React, { useState, useEffect } from "react";
import { Box, Container, Grid,createTheme as theme } from "@mui/material";
import CardList from "../../component/card";
import axios from "axios";
import DataEmpty from "../../component/dataEmpty";
import PhotoIcon from "@mui/icons-material/Photo";
import WorkIcon from "@mui/icons-material/Work";
import BeachAccessIcon from '@mui/icons-material/BeachAccess';
import Sidebar from "../../component/sidebar";
import useAsync from "../../hooks/useAsync";

export default function Apilist() {
    const apiUrl = process.env.REACT_APP_BASE_URL;
    const dataList = [
      {
        id: 1,
        avatar: <PhotoIcon/>
        ,
        name: "Photos",
        date: "Jan 9, 2014",
      },
      {
        id: 2,
        avatar: <WorkIcon/>,
        name: "Work",
        date: "Jan 7, 2014",
      },
      {
        id: 3,
        avatar: <BeachAccessIcon/>,
        name: "Vacation",
        date: "July 20, 2014",
      },
    ];
    // const callApi = () => {
    //   axios
    //     .get(`${apiUrl}`, {})
    //     .then((res) => setListData(res.data))
    //     .catch((err) => {
    //       console.log(err);
    //     });
    // };
    // // console.log(listdata);
  
    // useEffect(() => {
    //   callApi();
    // }, []);

    const data = useAsync(apiUrl);

    let createTheme = theme({
        palette: {
          primary: {
            main: process.env.REACT_APP_COLOR_PRIMARY,
          },
          secondary: {
            main: process.env.REACT_APP_COLOR_SECONDARY,
          },
          success: {
            main: process.env.REACT_APP_COLOR_TUMBNAIL,
          }
        },
      });
  
    return (
      <Container>
        <Grid container sx={{ mt: "20px" }}>
          <Grid xs={12} md={8}>
            {/* for the api list */}
            <Box sx={{ height: "50vh", mr: "20px" }}>
              <Grid container>
                {data.length > 0 ? (
                  data.map((item) => {
                    return <CardList items={item} />;
                  })
                ) : (
                  <DataEmpty />
                )}
              </Grid>
            </Box>
          </Grid>
  
          <Grid xs={12} md={4} sx={{ display: { xs: "none", md: "block" } }}>
                {dataList.length > 0
                  ? dataList.map((item, index) => {
                      return <Sidebar item={item} index={index} />;
                    })
                  : "no data"}
          </Grid>
          
        </Grid>
      </Container>
    );
  }